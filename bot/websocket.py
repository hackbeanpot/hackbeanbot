import logging
import websocket
import queue
import threading
import json


class SlackSocket(object):
    """
    A wrapper for a websocket.WebSocketApp object to interface with Slack
    """

    def __init__(self, rtm_url, kill_threads):
        """
        Open a websocket on the given Slack RTM URL, and prepare a message
        Queue for parsing and handling messages
        :param rtm_url: URL to use to connect to Slack's RTM
        :type rtm_url: str
        :param kill_threads: global event to kill threads
        :type kill_threads: threading.Event
        """
        self._kill_threads = kill_threads

        # Setup logging
        self._setup_logging()

        # Create an empty Queue
        self._queue = queue.Queue()

        # Establish an RTM connection
        self._connect(rtm_url)

        # Initialize threads
        self._logger.debug("Creating thread objects")
        self._sock_thread = threading.Thread(name="websocket",
                                             target=self._run_socket)
        self._handler_thread = threading.Thread(name="message_handler",
                                                target=self._handle_messages)

    def _setup_logging(self):
        """
        Setup the logging for the SlackSocket
        """
        self._logger = logging.getLogger("HackBeanBot.SlackSocket")
        self._logger.setLevel(logging.DEBUG)

    def _connect(self, rtm_url):
        """
        Establish a WebSocketApp for use with Slack's RTM API
        :param rtm_url: the websocket to connect to
        :type rtm_url: str
        """
        self._logger.debug("Connecting to websocket at %s", rtm_url)
        self._app = websocket.WebSocketApp(rtm_url,
                                           on_open=self._on_open,
                                           on_message=self._on_message,
                                           on_close=self._on_close,
                                           on_ping=self._on_ping,
                                           on_pong=self._on_pong,
                                           on_error=self._on_error)

    def _on_open(self, ws):
        pass

    def _on_message(self, ws, msg):
        """
        When a message is received from the websocket, add it to the Queue
        :param msg: the message from Slack
        :type msg: str
        """
        self._queue.put(msg)

    def _on_close(self, ws):
        pass

    def _on_ping(self, ws, frame):
        pass

    def _on_pong(self, ws, frame):
        pass

    def _on_error(self, ws, exception):
        pass

    def _run_socket(self):
        app_thread = threading.Thread(name="socket_app",
                                      target=self._app.run_forever,
                                      kwargs={'ping_interval': 60,
                                              'ping_timeout': 10})
        app_thread.start()
        while not self._kill_threads.is_set():
            continue
        self._app.close()
        while app_thread.is_alive():
            continue

    def _handle_messages(self):
        while not self._kill_threads.is_set():
            try:
                message = self._queue.get(timeout=10)
                self._logger.debug("Recieved message: %s", message)
                message = json.loads(message)
                if 'type' not in message:
                    pass
                elif message['type'] == 'message':
                    self._app.send(json.dumps(
                        {
                            'id': 1,
                            'type': 'message',
                            'channel': message['channel'],
                            'text': "ah! you scared me!"
                        }
                    ))
            except queue.Empty:
                continue

    def start(self):
        """
        Start listening on the socket and handling messages
        """
        self._logger.info("Starting threads")
        self._sock_thread.start()
        self._handler_thread.start()

        alive = True
        while alive:
            alive = (self._sock_thread.is_alive() or
                     self._handler_thread.is_alive())
        self._logger.debug("SlackSocket complete.")