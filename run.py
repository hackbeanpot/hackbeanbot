import argparse

from bot.controller import HackBeanBotController


parser = argparse.ArgumentParser(description='Run HackBeanBot')
parser.add_argument('--debug', action='store_true')


def main():
    args = parser.parse_args()
    hbb = HackBeanBotController(args.debug)
    hbb.run()


if __name__ == '__main__':
    main()