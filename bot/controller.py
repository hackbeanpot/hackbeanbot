import logging
import threading

import requests
import signal

from bot.websocket import SlackSocket
from bot.secrets import SLACK_API_TOKEN
from bot.utils import handlers


class HackBeanBotController(object):
    """
    The main controller for HackBeanBot. Initializes a websocket connection
    to utilize slack's RTM API, monitors incoming messages from slack, and
    runs the Flask app for webhooks.
    """

    def __init__(self, debug):
        """
        Create and initialize this HackBeanBotController:
            - establish a connection to slack
            - test the api's response
            - test the authorization of the slack token
            - send a startup message to admin
            - start the RTM process
            - create the WebSocketApp
        """
        # Initialize values
        self._slack_session = requests.session()
        self._token = SLACK_API_TOKEN
        self._token_data = {'token': self._token}

        # Set up logging
        self._logging_setup(debug)

        try:
            # Test the API connection
            self.api_test()
            # Test the authorization of the SLACK_API_TOKEN
            self.auth_test()
        except requests.RequestException as reqEx:
            self._logger.error("Error while testing API access: %s",
                               str(reqEx))
            raise RuntimeError("Cannot initialize HackBeanBotController") from reqEx

        # Generate the channel-name to channel-id mapping
        self._channel_map = self._generate_channel_map()

        # Load in some additional ones as desired:
        self._channel_map['bots'] = 'G04G3R9MB'

        # Send startup message to admin
        self._api_message('bots', "Initializing HackBeanBot, hold on to your ass")

        # Create threading event
        self._kill_threads = threading.Event()

        # Create a websocket
        self._websocket = SlackSocket(self._get_rtm_url(),
                                      self._kill_threads)

        # Set up threading objects and handlers
        signal.signal(signal.SIGINT, self._sigint_handler)
        self._socket_thread = threading.Thread(name="slacksocket",
                                               target=self._websocket.start)
        self._flask_thread = threading.Thread(name="flask",
                                              target=None)

    def _logging_setup(self, debug):
        """
        Gets the HackBeanBot logger, sets its level according to debug, and
        adds handlers
        :param debug: if `True`, level is DEBUG
        :type debug: bool
        """
        self._logger = logging.getLogger("HackBeanBot")

        # set the console logging level
        if debug:
            self._logging_level = logging.DEBUG
        else:
            self._logging_level = logging.INFO

        self._logger.setLevel(logging.DEBUG)

        # Set up logging handlers
        # SlackHandler for logging warnings and error messages to slack
        slack_handler = handlers.SlackHandler()
        slack_handler.setLevel(logging.WARNING)
        self._logger.addHandler(slack_handler)

        # Formatter to use for console and file logging
        formatter = logging.Formatter(
            "[%(asctime)s] - %(name)s - %(threadName)s: [%(levelname)s] %(message)s"
        )

        # StreamHandler for logging to console
        stream_handler = logging.StreamHandler()
        stream_handler.setLevel(self._logging_level)
        stream_handler.setFormatter(formatter)
        self._logger.addHandler(stream_handler)

        # FileHandler for logging to file
        file_handler = logging.FileHandler('hackbeanbot.log')
        file_handler.setLevel(logging.DEBUG)
        file_handler.setFormatter(formatter)
        self._logger.addHandler(file_handler)

        self._logger.info("Logging setup successfully")

    def _generate_channel_map(self):
        """
        Generates a mapping of channel names to channel ids, including
        groups and IMs.
        :return: dict
        """
        channel_map = dict()

        # Grab normal channel lists
        response = self._slack_session.post("https://slack.com/api/channels.list",
                                            data=self._token_data)
        for channel in response.json()['channels']:
            channel_map[channel['name']] = channel['id']

        # Grab direct message lists
        response = self._slack_session.post("https://slack.com/api/im.list",
                                            data=self._token_data)
        for im in response.json()['ims']:
            channel_map[im['user']] = im['id']

        return channel_map

    def _api_message(self, channel, message):
        """
        Send the given message to the given channel
        :param channel: channel name
        :param message: message text
        """
        data = {
            'token': self._token,
            'as_user': True,
            'channel': self._channel_map[channel],
            'text': message
        }
        self._slack_session.post("https://slack.com/api/chat.postMessage",
                                 data=data)

    def _get_rtm_url(self):
        """
        Grab an RTM URL
        """
        self._logger.debug("Getting RTM URL from Slack")

        response = self._slack_session.post("https://slack.com/api/rtm.start",
                                            data=self._token_data)
        return response.json()['url']

    def api_test(self):
        """
        Access slack's api.test endpoint to see if the API is live
        """
        self._logger.debug("Testing connection to Slack API")

        response = self._slack_session.post("https://slack.com/api/api.test")
        if not response:
            raise requests.RequestException("API returned status code: ",
                                            response.status_code)
        elif not response.json()['ok']:
            raise requests.RequestException("API returned 'not ok'")

    def auth_test(self):
        """
        Access slack's auth.test endpoint to see if the set API token is
        properly authed
        """
        self._logger.debug("Testing authorization with Slack API")

        response = self._slack_session.post("https://slack.com/api/auth.test",
                                            data=self._token_data)
        if not response:
            raise requests.RequestException("Could not access API")
        elif not response.json()['ok']:
            raise requests.RequestException("User with token %s is not authed" %
                                            self._token)

    def _sigint_handler(self, signum, stack_frame):
        """
        Sets the kill_threads Event to signal all threads they should kill
        :param signum: the signal number received
        :type signum: int
        :param stack_frame: the current stack frame
        :type stack_frame: frame
        :return:
        """
        self._logger.warn("SIGINT Received. Shutting down.")
        self._kill_threads.set()
        self._wait_for_threads()

    def _wait_for_threads(self):
        """
        Wait for all threads to complete, and log completion.
        """
        alive = True
        while alive:
            alive = (self._socket_thread.is_alive() or
                     self._flask_thread.is_alive())

        self._logger.info("HackBeanBot exited.")

    def run(self):
        """
        Create and start threads:
            - flask app for webhooks
            - websocket for RTM
        """
        self._socket_thread.run()