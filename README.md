# TODO

- Eventbrite/slack integration (i.e. when attendees sign up, when attendees send support messages, etc)
- github/slack integration (in cooler ways, i.e. issue tracking, etc)
- deadlines/GCal integration ("/deadline add {name} {desc} {assignee(s)}" will add things to GCal, autoremind, etc)
- better GDocs integration
- event shift management
- /pokemon to get a random pokemon from frankenpokemon.me or elsewhere