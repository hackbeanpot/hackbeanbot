import logging
import requests
import json

from bot.secrets import SLACK_API_TOKEN


class SlackHandler(logging.Handler):

    def __init__(self):
        """
        Initialize the SlackHandler with a data dict and a session object.
        Note that this functionality remains independent from the rest of HBB
        for the sake of being able to notify slack without needing anything
        else to be working
        """
        super().__init__()
        self._session = requests.session()
        self._data = {
            'token': SLACK_API_TOKEN,
            'channel': 'G04G3R9MB',
            'text': 'HackBeanBot Error',
            'as_user': True,
            'attachments': None
        }

    def emit(self, record):
        """
        Post the record to the bots group
        :param record: logging message to post
        :type record: logging.LogRecord
        """
        if record.levelno == logging.WARNING:
            color = 'warning'
            title = 'Warning'
        elif record.levelno == logging.ERROR:
            color = 'danger'
            title = 'Error'
        else:
            color = '#000000'
            title = 'Critical'

        # format the error in cool ways
        attachments = [
            {
                'fallback': "Logging " + color,
                'color': color,
                'title': title,
                'fields': [
                    {
                        'title': 'File',
                        'value': record.pathname,
                        'short': True
                    },
                    {
                        'title': 'Line',
                        'value': record.lineno,
                        'short': True
                    }
                ],
                'text': record.getMessage()
            }
        ]

        # encode the attachments array as JSON and add it to the data
        self._data['attachments'] = json.dumps(attachments)

        # post it to slack
        response = self._session.post("https://slack.com/api/chat.postMessage",
                           data=self._data)